INSTALLATION
============

Building from source
~~~~~~~~~~~~~~~~~~~~

The VMOD is built on a system where an instance of Varnish is
installed, and the auto-tools will attempt to locate the Varnish
instance, and then pull in libraries and other support files from
there.

Quick start
-----------

This sequence should be enough in typical setups:

1. ``./bootstrap``  (for git-installation)
3. ``make``
4. ``make check`` (regression tests)
5. ``make install`` (may require root: sudo make install)

Alternative configs
-------------------

If you have installed Varnish to a non-standard directory, call
``autogen.sh`` and ``configure`` with ``PKG_CONFIG_PATH`` pointing to
the appropriate path. For example, when varnishd configure was called
with ``--prefix=$PREFIX``, use::

  PKG_CONFIG_PATH=${PREFIX}/lib/pkgconfig
  ACLOCAL_PATH=${PREFIX}/share/aclocal
  export PKG_CONFIG_PATH ACLOCAL_PATH
