..
.. NB:  This file is machine generated, DO NOT EDIT!
..
.. Edit ./vmod_acltools.vcc and run make instead
..

.. role:: ref(emphasis)

=============
vmod_acltools
=============

----------------------------------------
Varnish Access Control List (ACL) Module
----------------------------------------

:Manual section: 3

SYNOPSIS
========

.. parsed-literal::

  import acltools [as name] [from "path"]
  
  ACL dyn_single(STRING ipmask, [ACL fallback], BOOL resolve, INT min_mask_ip4, INT min_mask_ip6)
  
DESCRIPTION
===========

This vmod contains additional functions for Access Control Lists
(ACLs).

.. _acltools.dyn_single():

ACL dyn_single(STRING ipmask, [ACL fallback], BOOL resolve, INT min_mask_ip4, INT min_mask_ip6)
-----------------------------------------------------------------------------------------------

::

   ACL dyn_single(
      STRING ipmask,
      [ACL fallback],
      BOOL resolve=0,
      INT min_mask_ip4=0,
      INT min_mask_ip6=0
   )

Returns a dynamic ACL **for immediate use** (see **NOTE** below) with
a single entry constructed from the *ipmask* string, which must be of
format::

  <ip>[/<mask>]

or, for compatibility with built-in static ACLs::

  "<ip>"[/<mask>]

.. _`std.ip()`: https://varnish-cache.org/docs/trunk/reference/vmod_std.html#std-ip

*<ip>* undergoes the same parsing as the *s* argument of
`std.ip()`_. If *<ip>* is an IPv4 address, *<mask>*, if present, must
be an integer between *min_mask_ip4* and 32. If *<ip>* is an IPv6 address,
*<mask>*, if present, must be an integer between *min_mask_ip6* and 128. If
*<mask>* is not present, it defaults to the maximum mask (32 or 128,
respectively).

If *<ip>* is not a valid address or *<mask>* is invalid for the
address, the *fallback* argument is returned, if present. If
*fallback* is not given with invalid input, the ACL match will always
fail.

If the *resolve* argument is ``true``, *<ip>* will be resolved as a
DNS name.

*min_mask_ip4* must be an integer between 0 and 32, inclusively.

*min_mask_ip6* must be an integer between 0 and 128, inclusively.

For both *min_mask_* arguments, a VCL error is triggered if they are
out of range. Both default to zero.

Example
    ::

        import acltools;

        sub vcl_recv {
	    if (client.ip ~ acltools.dyn_single(req.http.acl) {
	        return (synth(200, "matched"));
	    } else {
	        return (synth(403, "denied"));
	    }
        }

.. _`current limitations`: https://github.com/varnishcache/varnish-cache/issues/3904

**NOTE** Due to `current limitations`_ in the Varnish-Cache APIs,
there only ever exists one ACL created by this function, and any match
is always on the ACL last created. In particular, ACLs created by this
function can not be saved in variables for later use, they always need
to be matched immediately.

SEE ALSO
========

vcl\(7),
varnishd\(1)
