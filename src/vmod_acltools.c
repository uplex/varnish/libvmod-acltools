#include "config.h"

#include <limits.h>

#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>

#include <netdb.h>

#include <cache/cache.h>
#include <vsa.h>
#include <vtcp.h>



#include "vcc_acltools_if.h"

/* XXX vcc_interface.h */
typedef int acl_match_f(VRT_CTX, const VCL_IP);

struct vrt_acl {
	unsigned	magic;
#define VRT_ACL_MAGIC	0x78329d96
	acl_match_f	*match;
	const char	*name;
};

/* XXX vss.h */
const struct suckaddr *VSS_ResolveFirst(void *dst,
    const char *addr, const char *port,
    int family, int socktype, int flags);


int acl_fail_f(VRT_CTX, const VCL_IP ip)
{

	(void) ctx;
	(void) ip;

	return (0);
}

struct vrt_acl acl_fail = {
	.magic = VRT_ACL_MAGIC,
	.match = acl_fail_f,
	.name = "acltools.fail"
};

struct acl_dyn_single {
	unsigned		magic;
#define ACL_DYN_SINGLE_MAGIC	0xb30f5ac9
	unsigned		mask;
	struct vrt_acl		acl;
	char			vsa[];
};

int dyn_single_match_f(VRT_CTX, const VCL_IP ip)
{
	struct vmod_priv *priv_task;
	const struct acl_dyn_single *ads;
	unsigned const char *aip, *aacl;
	unsigned b, m;
	int fip, facl;

	priv_task = VRT_priv_task_get(ctx, dyn_single_match_f);
	if (priv_task == NULL) {
		VRT_fail(ctx, "No priv_task for dynamic ACL match");
		return (0);
	}

	CAST_OBJ_NOTNULL(ads, priv_task->priv, ACL_DYN_SINGLE_MAGIC);

	fip = VSA_GetPtr(ip, &aip);
	facl = VSA_GetPtr((struct suckaddr *)ads->vsa, &aacl);

	if (fip != facl) {
		VSLb(ctx->vsl, SLT_Error,
		    "acltools.dyn_single: address family mismatch: "
		    "ip: %s, acl: %s",
		    fip == PF_INET ? "IPv4" : "IPv6",
		    facl == PF_INET ? "IPv4" : "IPv6");
		return (0);
	}

	b = ads->mask / 8;
	m = ads->mask % 8;

	if (memcmp(aip, aacl, b))
		goto nomatch;
	if (m == 0)
		goto match;
	m = 0xff00 >> m;
	m &= 0xff;
	if ((aip[b] & m) == (aacl[b] & m))
		goto match;
  nomatch:
	VSLbs(ctx->vsl, SLT_VCL_acl, TOSTRANDS(2,
		"NO_MATCH ",
		ads->acl.name));
	return (0);
  match:
	VSLbs(ctx->vsl, SLT_VCL_acl, TOSTRANDS(2,
		"MATCH ",
		ads->acl.name));
	return (1);
}

#define skipws(s) while(						\
	    *(s) == ' ' ||						\
	    *(s) == '\t'||						\
	    *(s) == '\r'||						\
	    *(s) == '\n')						\
		(s)++

VCL_ACL
vmod_dyn_single(VRT_CTX, struct VARGS(dyn_single)*args)
{
	struct acl_dyn_single *ads;
	const struct suckaddr *vsa;
	char *p, *pp, ipmask[VTCP_ADDRBUFSIZE + 4];
	const char *a;
	unsigned const char *addr;
	struct vmod_priv *priv_task;
	unsigned long mask = ULONG_MAX;
	unsigned minmask, maxmask;
	uintptr_t sn;
	int quot = 0;
	int fam;
	VCL_ACL fb;

	if (args->min_mask_ip4 < 0 || args->min_mask_ip6 < 0) {
		VRT_fail(ctx, "min_mask_* arguments must not be negative");
		return (&acl_fail);
	}
	if (args->min_mask_ip4 > 32) {
		VRT_fail(ctx, "min_mask_ip4 value too largs (%ld)",
		    args->min_mask_ip4);
		return (&acl_fail);
	}
	if (args->min_mask_ip6 > 128) {
		VRT_fail(ctx, "min_mask_ip6 value too largs (%ld)",
		    args->min_mask_ip6);
		return (&acl_fail);
	}

	if (args->valid_fallback && args->fallback != NULL &&
	    args->fallback->magic == VRT_ACL_MAGIC)
		fb = args->fallback;
	else
		fb = &acl_fail;

	if (args->ipmask == NULL) {
		VSLb(ctx->vsl, SLT_Error, "acltools.dyn_single: empty ipmask");
		return (fb);
	}
	a = args->ipmask;
	skipws(a);
	if (a[0] == '\0') {
		VSLb(ctx->vsl, SLT_Error, "acltools.dyn_single: empty ipmask");
		return (fb);
	}

	skipws(a);
	if (*a == '"') {
		a++;
		quot = 1;
	}

	if (strlen(a) > sizeof ipmask - 1) {
		VSLb(ctx->vsl, SLT_Error,
		    "acltools.dyn_single: too long: %s",
		    args->ipmask);
		return (fb);
	}

	(void) strcpy(ipmask, a);
	a = NULL;

	p = ipmask;
	if (quot) {
		p = strchr(p, '"');
		if (p == NULL) {
			VSLb(ctx->vsl, SLT_Error,
			    "acltools.dyn_single: unmatched quote for %s",
			    args->ipmask);
			return (fb);
		}
		*p = '\0';
		p++;
		skipws(p);
	}
	p = strchr(p, '/');
	if (p != NULL) {
		*p = '\0';
		p++;	// ok to point to \0 now
		skipws(p);
		pp = NULL;
		mask = strtoul(p, &pp, 0);
		if (pp != NULL) {
			skipws(pp);
			if (*pp != '\0') {
				VSLb(ctx->vsl, SLT_Error,
				    "acltools.dyn_single: "
				    "garbage after mask in %s",
				    args->ipmask);
				return (fb);
			}
		}
	}

	sn = WS_Snapshot(ctx->ws);
	ads = WS_Alloc(ctx->ws, sizeof *ads + vsa_suckaddr_len);
	if (ads == NULL) {
		VRT_fail(ctx, "acltools.dyn_single: insufficient workspace");
		return (fb);
	}
	INIT_OBJ(ads, ACL_DYN_SINGLE_MAGIC);

	vsa = VSS_ResolveFirst(
	    ads->vsa, ipmask, "0",
	    AF_UNSPEC, SOCK_STREAM,
	    args->resolve ? 0 : AI_NUMERICHOST|AI_NUMERICSERV);
	if (vsa == NULL) {
		VSLb(ctx->vsl, SLT_Error,
		    "acltools.dyn_single: conversion failed for %s",
		    ipmask);
		WS_Reset(ctx->ws, sn);
		return (fb);
	}
	assert((void *)vsa == (void *)ads->vsa);

	fam = VSA_GetPtr(vsa, &addr);
	switch (fam) {
	case PF_INET:
		minmask = args->min_mask_ip4;
		maxmask = 32;
		break;
	case PF_INET6:
		minmask = args->min_mask_ip6;
		maxmask = 128;
		break;
	default:
		WRONG("address family");
	}

	if (mask == ULONG_MAX)
		mask = maxmask;
	else if (mask > maxmask) {
		VSLb(ctx->vsl, SLT_Error, "acltools.dyn_single: "
		    "mask %lu too long for %s",
		    mask, args->ipmask);
		WS_Reset(ctx->ws, sn);
		return (fb);
	}
	else if (mask < minmask) {
		VSLb(ctx->vsl, SLT_Error, "acltools.dyn_single: "
		    "mask %lu shorter than minimum %d for %s",
		    mask, minmask, args->ipmask);
		WS_Reset(ctx->ws, sn);
		return (fb);
	}

	ads->mask = mask;
	ads->acl.magic = VRT_ACL_MAGIC;
	ads->acl.match = dyn_single_match_f;
	ads->acl.name = args->ipmask;

	priv_task = VRT_priv_task(ctx, dyn_single_match_f);
	if (priv_task == NULL) {
		VRT_fail(ctx, "acltools.dyn_single: no task available");
		return (fb);
	}

	priv_task->priv = ads;

	return (&ads->acl);
}
