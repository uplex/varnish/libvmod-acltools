=================================================
Access Control List (ACL) Tools for Varnish-Cache
=================================================

.. role:: ref(emphasis)

This project provides additional tools for Access Control Lists (ACLs)
in Varnish-Cache.

PROJECT RESOURCES
=================

* The primary repository is at
  https://code.uplex.de/uplex-varnish/libvmod-acltools

  This server does not accept user registrations, so please use ...

* the mirror at https://gitlab.com/uplex/varnish/libvmod-acltools for issues,
  merge requests and all other interactions.

INTRODUCTION
============

For now, this project adds a single missing function to Varnish-Cache:
To create dynamic Access Control Lists with a single entry from
strings in VCL.

Example::

  import acltools;

  sub vcl_recv {
    if (client.ip ~ acltools.dyn_single(req.http.acl)) {
      return (synth(200, "matched"));
    } else {
      return (synth(403, "denied"));
    }
  }

In this example, an ACL gets created from the `acl` request header
header and is then used for a match of `client.ip`.

The acl entry is expected in the format::

  <ip>[/<mask>]

.. _vmod_acltools.man.rst: src/vmod_acltools.man.rst

See the :ref:`vmod_acltools(3)` man page for more details. If you are
reading this file on line, it should also be available as
`vmod_acltools.man.rst`_.

INSTALLATION
============

.. _INSTALL.rst: https://code.uplex.de/uplex-varnish/libvmod-acltools/blob/master/INSTALL.rst

See `INSTALL.rst`_.

SUPPORT
=======

.. _gitlab.com issues: https://gitlab.com/uplex/varnish/libvmod-acltools/-/issues

To report bugs, use `gitlab.com issues`_.

For enquiries about professional service and support, please contact
info@uplex.de\ .

CONTRIBUTING
============

.. _merge requests on gitlab.com: https://gitlab.com/uplex/varnish/libvmod-acltools/-/merge_requests

To contribute to the project, please use `merge requests on gitlab.com`_.

To support the project's development and maintenance, there are
several options:

.. _paypal: https://www.paypal.com/donate/?hosted_button_id=BTA6YE2H5VSXA

.. _github sponsor: https://github.com/sponsors/nigoroll

* Donate money through `paypal`_. If you wish to receive a commercial
  invoice, please add your details (address, email, any requirements
  on the invoice text) to the message sent with your donation.

* Become a `github sponsor`_.

* Contact info@uplex.de to receive a commercial invoice for SWIFT payment.
